# README #

This is the README for juggle, a small game written in Lua with the LÖVE2D engine.

### What is this repository for? ###

juggle is a small game in which players must attempt to keep one or more objects from leaving the screen by creating barriers along the screen's borders. Presently, juggle is a solo project, being maintained by Chris Alderton ([Chia L'Étranger](https://chikun.net/members/chia/) of [chikun](https://chikun.net)) as an exercise in coding and game creation.
Current version: 0.01

### How do I get set up? ###

You'll need the following to run juggle:

* LÖVE version 0.10.0 (available from [here](http://love2d.org))

* the juggle source code


Once LÖVE is installed and set up, you can run juggle as follows:

Windows:

Navigate to the top level of the juggle directory (path\to\juggle\) and then run the command 
```
#!batch

love ./
```

It's possible that this won't work. In this case, either add the directory you installed LÖVE in to your PATH variable, or else run the command as 

```
#!batch

"path\to\LÖVE\love.exe" ./
```



Linux:

Navigate to the top level of the juggle directory (/path/to/juggle/) and then run the command 
```
#!bash

love ./
```



OS X:

From a Terminal window, run the command 
```
#!bash

open -n -a love "~/path/to/juggle"
```



Android/iOS:

Unfortunately, juggle doesn't yet have mobile support. This section of the README will be updated as necessary - watch this space.


### Contribution guidelines ###

At present, juggle is a solo project and is not seeking any other contributors. However, if you want to create a fork and write some code, I won't stop you! At some point in the future, assuming the project carries on long enough, other contributors may be necessary.

### Who do I talk to? ###

The only current point of contact is me. You can contact me here, or at christopher.alderton@hotmail.com with any queries.