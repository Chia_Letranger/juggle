-- Configuration for a new game
function love.conf(t)
    t.version       = "0.10.0"
    t.window.title  = "juggle"
    --t.window.icon   = ""
    t.window.width  = 800
    t.window.height = 600
    t.window.resizable = false
end
