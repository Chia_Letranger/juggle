ball = {}
makeBall        = true
ballTimerMax    = 0
ballTimer       = 0


ball.update = function(dt)
    if #ball == 0 then
        makeBall = true
    end

    if makeBall then
        newBall = {
                w   = 20,
                h   = 20,
                x   = lm.random(10, g.getWidth() - 30),
                y   = g.getHeight() / 2,
                c   = {
                    r   = lm.random(0, 200),
                    g   = lm.random(0, 200),
                    b   = lm.random(0, 200)
                },
                xv  = lm.random(-20, 20),
                yv  = lm.random(20),
                l   = string.char(lm.random(97, 122))
        }
        table.insert(ball, newBall)
        makeBall        = false
        ballTimerMax    = ballTimerMax + 5
        ballTimer       = 0
    end

    ballTimer = ballTimer + dt

    if ballTimer >= ballTimerMax then
        makeBall = true
    end

    for i, ball in ipairs(ball) do
        if ball.y > 400 then
            table.remove(ball, i)
            ballTimermax    = ballTimerMax - 5
        end
        ball.y  = ball.y - (ball.yv * dt)
        ball.x  = ball.x + (ball.xv * dt)
        ball.yv = ball.yv - (9.8 * dt)
        if ball.x < 0 or ball.x >= g.getWidth() - ball.w then
            ball.xv = -ball.xv
        end
    end

end

ball.draw = function()
    for i, ball in ipairs(ball) do
        g.setColor(ball.c.r, ball.c.g, ball.c.b)
        g.rectangle("fill", ball.x, ball.y, ball.w, ball.h)
        g.setColor(0, 0, 0)
        g.print(string.upper(ball.l), ball.x + 3, ball.y, 0)
    end
end

ball.launch = function(key)
    for i, ball in ipairs(ball) do
        if ball.l == key and ball.y > (4 * g.getHeight()) / 5 then
            ball.yv = 70
        end
    end
end
