function love.load()
    require "code/startup/start"
    g.setBackgroundColor(255, 255, 255)
    g.setNewFont(20)
end

function love.update(dt)
    if dt < 1 / 15 then dt = 1 / 15 end
    ball.update(dt)
end

function love.draw()
    g.setColor(0, 0, 0)
    ball.draw()
end

function love.keypressed(key)
    if key == "escape" then
        e.quit()
    else
        ball.launch(key)
    end
end

