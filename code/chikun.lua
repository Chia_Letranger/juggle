-- Important functions needed in most games

-- Returns the rounded value of a given number
-- to 'dp' decimal points
function math.round(val, dp)
    dp = 10 ^ (dp or 0)
    return math.floor(val * dp + 0.5) / dp
end

-- Returns the sign of a given number
function math.sign(val)
    return val > 0 and 1 or val < 0 and -1 or 0
end

-- Returns the distance between two points
function math.dist(x1, y1, x2, y2)
    return ((x2 - x1) ^ 2 + (y2 - y1) ^ 2) ^ 0.5
end

-- Returns the shortest distance between point3 and a line formed by
-- point1 and point 2. Returns -1 if point1 == point2.
function math.distPointToLine(point1, point2, point3)
    local line = {point1, point2}
    if (line[1].x == line[2].x) and (line[1].y == line[2].y) then
        return -1
    end
    local d = math.dist(line[1].x, line[1].y, line[2].x, line[2].y)
    local t = ((point3.x - line[1].x) * (line[2].x - line[1].x) +
            (point3.y - line[1].y) * (line[2].y - line[1].y)) / (d ^ 2)
    -- Projection
    local projection = { }
    projection.x = line[1].x + t * (line[2].x - line[1].x)
    projection.y = line[1].y + t * (line[2].y - line[1].y)
    return math.dist(projection.x, projection.y, point3.x, point3.y)
end

chikun = { }

-- Returns true or false depending on if two rectangles are overlapping
function chikun.overlapRect(obj1, obj2)
    returnValue = false
    if obj1.x + obj1.width > obj2.x and
        obj2.x + obj2.width > obj1.x and
        obj1.y + obj1.height > obj2.y and
        obj2.y + obj2.height > obj1.y then
        returnValue = true
    end
    return returnValue
end

-- Returns true or false depending on if two circles are overlapping
function chikun.overlapCircle(obj1, obj2)
    returnValue = false
    if (obj1.radius + obj2.radius > math.dist(obj1.x, obj1.y, obj2.x, obj2.y)) then
        returnValue = true
    end
    return returnValue
end

-- Shorthand variable names
c   = chikun
l   = love
a   = l.audio
e   = l.event
f   = l.font
fs  = l.filesystem
g   = l.graphics
img = l.image
k   = l.keyboard
lm  = l.math
m   = l.mouse
p   = l.physics
s   = l.sound
sys = l.system
t   = l.timer
th  = l.thread
w   = l.window
