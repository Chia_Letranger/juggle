--[[
    Executed on program startup, loads important libraries.
  ]]--

require "code/chikun"   -- Essential functions

require "code/startup/loadGFX"  -- Load graphics
require "code/startup/loadBGM"  -- Load music
require "code/startup/loadSFX"  -- Load sound effects

require "src/ball"              -- Load ball
